import {Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCard () {

	return (
		<Row className="mb-3">
			<Col>
				<Card>
					<Card.Title className="p-3">Sample Course</Card.Title>

				  	<Card.Body>
				    
				    <Card.Subtitle className="mb-2 text-muted">Description:</Card.Subtitle>
				    <Card.Text>
				    	This is a sample course offering.
				    </Card.Text>

				    <Card.Subtitle className="mb-2 text-muted">Price:</Card.Subtitle>
				    <Card.Text>
				    	Php 40,000
				    </Card.Text>

				    <Button href="#enroll">Enroll</Button>

				  </Card.Body>
				</Card>
			</Col>
		</Row>

	)

}